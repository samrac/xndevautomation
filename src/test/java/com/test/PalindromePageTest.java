package com.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.data.BrowserName;
import com.data.Devices;
import com.pages.PalindromePage;
import com.utilities.Utilities;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners(com.listeners.MyCustomListeners.class)
public class PalindromePageTest {
	PalindromePage palindromePage;

	@BeforeMethod(description = "sets up the requiremnt for the tests")
	public void setup() {
		palindromePage = new PalindromePage();
		palindromePage.launchBrowser(BrowserName.CHROME);
		palindromePage.setScreenSize(Devices.PC);
		palindromePage.loadPage(Utilities.readConfigFile("URL"));

	}

	@Description("verifies Palindrome  feature is working or not")
	@Severity(SeverityLevel.NORMAL)
	@Story("Verify the Palindrome Feature")
	@Test(testName = "Palindrome Test", description = "verifies Palindrome  feature is working or not", groups = {
			"e2e,smoke" })
	public void palindromeTest() {
		Assert.assertEquals(palindromePage.checkIfPalindrome("111"), true);

	}

	@Description("verifies Anargam  feature is working or not")
	@Severity(SeverityLevel.NORMAL)
	@Story("Verify the Anargam Feature")
	@Test(testName = "Anargam Test", description = "verifies Anargam  feature is working or not", groups = {
			"e2e,smoke" })
	public void anargamTest() {
		List<String> data = palindromePage.checkIfAnargam("111");
	}

//	@AfterMethod(description = "Close the browser sessions")
//	public void tearDown() {
//		palindromePage.quitBrowserSession();
//	}

}
