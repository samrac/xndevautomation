package com.pages;

import java.util.ArrayList;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import com.wrapper.BrowserUtility;

public class PalindromePage extends BrowserUtility {

	private static final By ORIGINAL_WORD_TEXTBOX_LOCATOR = By.id("originalWord");
	private static final By SUBMIT_BUTTON_LOCATOR = By.id("button1");
	private static final By ANARGAM_WORD_TEXTBOX_LOCATOR = By.id("anagramWord");
	private static final By ANARGAM_SUBMIT_BUTTON_LOCATOR = By.id("button2");
	private static final By PALINDROME_OUTPUT_LOCATOR = By.id("palindromeResult");
	private static final By ANARGAM_OUTPUT_LOCATOR = By.id("anagramResult");

	public boolean checkIfPalindrome(String text) {
		enterText(ORIGINAL_WORD_TEXTBOX_LOCATOR, text);
		clickOnElement(SUBMIT_BUTTON_LOCATOR);
		String response = getTextFromElement(PALINDROME_OUTPUT_LOCATOR);
		if (response.contains("Yes")) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<String> checkIfAnargam(String text) {
		enterText(ANARGAM_WORD_TEXTBOX_LOCATOR, text);
		clickOnElement(ANARGAM_SUBMIT_BUTTON_LOCATOR);
		String response = getTextFromElement(ANARGAM_OUTPUT_LOCATOR);
		Scanner s = new Scanner(response);
		ArrayList<String> data = new ArrayList<String>();
		while (s.hasNextLine()) {
			String line = s.nextLine();
			System.out.println(line);
			data.add(line);
		}
		data.remove(0);
		return data;
	}

}
