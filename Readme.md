# Automation Solution

This project uses java programming language along with maven to manage dependencies:

##Repo Link: 
Markup : [Gitlab](https://gitlab.com/testautomationacademy/atbatchnov2020/interviewquestion/automationprojectsolution.git)

## Add dependencies:
Add it in pom.xml

1. Selenium WedDriver - UI Automation
2. WeDriver Manager - Managing the driver files
3. Test NG- For writing e2e, tests
4. Rest Assured - Making API requests
5. Open CSV - Read Data from CSV file
6. Apache POI - Read Data from XLS and XLSX file
7. Extent Reports - Generating HTML Reports in report folder
8. Commons IO - working with files and folders
9. Allure Report (Optional) - generating Allure Reports

          
##Add dependencies:
```bash
<dependecies>
.
.
.

<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>3.141.59</version>
		</dependency>
</dependecies>

```